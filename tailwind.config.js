const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import("@types/tailwindcss/tailwind-config").TailwindConfig } */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#486DDA',
        main: '#065C81',
        inactive: '#D1D3E2',
        secondary: '#3A3A3A',
        top: '#F0F4FF',
        circle: '#54A5FF',
        line: '#28C8DE',
        circle2: '#7391DE',
        box: '#05ACAE',
        grey: '#707070',
        circlefooter: '#EDF5FD',
        boxfooter: '#096993',
        appwrapper: '#F7F7F7',
      },
      fontFamily: {
        sans: ['"Inter var"', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
